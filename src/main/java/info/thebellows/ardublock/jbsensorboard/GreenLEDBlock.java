package info.thebellows.ardublock.jbsensorboard;

import com.ardublock.translator.Translator;

public class GreenLEDBlock extends AbstractLEDBlock {
	public GreenLEDBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label, LEDColor.GREEN);
	}
}

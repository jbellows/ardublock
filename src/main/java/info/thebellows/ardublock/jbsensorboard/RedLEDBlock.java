package info.thebellows.ardublock.jbsensorboard;

import com.ardublock.translator.Translator;

public class RedLEDBlock extends AbstractLEDBlock {
	public RedLEDBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label, LEDColor.RED);
	}
}

package info.thebellows.ardublock.jbsensorboard;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.NumberBlock;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class ButtonBlock extends TranslatorBlock {
		
	public ButtonBlock(Long blockId, Translator translator,	String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		if (!(translatorBlock instanceof NumberBlock)) {
			throw new BlockException(this.blockId, "the value must be a number block");
		}
		
		String button = translatorBlock.toCode();
		String ret = null;
		
		if (button.equals("3")) {
			ret = "(analogRead(A3) > 700 && analogRead(A3) < 825)";
		} else if (button.equals("2")) {
			ret = "(analogRead(A3) > 450 && analogRead(A3) < 575)";
		} else if (button.equals("1")) {
			ret = "(analogRead(A3) > 200 && analogRead(A3) < 300)";
		} else {
			throw new BlockException(this.blockId, "the value must be 1, 2, or 3");
		}
		
		return ret;
	}

}

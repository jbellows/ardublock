package info.thebellows.ardublock.jbsensorboard;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

public class TemperatureBlock extends TranslatorBlock {
	private String fileLocation = "info/thebellows/ardublock/jbsensorboard/temperature.c";

	public TemperatureBlock(Long blockId, Translator translator,
			String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
		// TODO Auto-generated constructor stub
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException {
		URL fileURL = BlockGenus.class.getClassLoader().getResource(fileLocation);
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(fileURL.openStream()));
			String command = "", line;
			while((line = in.readLine()) != null) {
				command = command.concat(line + "\n");
			}
			
			translator.addDefinitionCommand(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
	
		String ret = "(((analogRead(2)/1024.0) * readVcc()) - 63)";
		return ret;
	}

}

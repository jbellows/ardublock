package info.thebellows.ardublock.jbsensorboard;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.DigitalOffBlock;
import com.ardublock.translator.block.DigitalOnBlock;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

abstract public class AbstractLEDBlock extends TranslatorBlock {
	protected enum LEDColor {RED, GREEN, BLUE, WHITE};
	protected LEDColor color;
	
	public AbstractLEDBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label, LEDColor color)
	{
		super(blockId, translator, codePrefix, codeSuffix, label);
		this.color = color;
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		if (!(translatorBlock instanceof DigitalOffBlock) && !(translatorBlock instanceof DigitalOnBlock))
		{
			throw new BlockException(this.blockId, "the status needs to be a ON or OFF value");
		}
		
		String socketValue = translatorBlock.toCode();
		int pinNumber = 0;
		switch(color) {
			case RED:
				pinNumber = 9;
				break;
			case GREEN:
				pinNumber = 6;
				break;
			case BLUE:
				pinNumber = 5;
				break;
			case WHITE:
				pinNumber = 3;
				break;
		}
		
		translator.addSetupCommand("pinMode(" + pinNumber + ", OUTPUT);");
		
		String ret = "digitalWrite(" + pinNumber + ", " + socketValue + ");";
		
		return ret;
	}
}

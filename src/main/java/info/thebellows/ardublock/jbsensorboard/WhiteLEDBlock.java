package info.thebellows.ardublock.jbsensorboard;

import com.ardublock.translator.Translator;

public class WhiteLEDBlock extends AbstractLEDBlock {
	public WhiteLEDBlock(Long blockId, Translator translator, String codePrefix, String codeSuffix, String label)
	{
		super(blockId, translator, codePrefix, codeSuffix, label, LEDColor.WHITE);
	}
}

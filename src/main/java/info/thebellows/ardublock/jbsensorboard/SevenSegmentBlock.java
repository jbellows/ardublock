package info.thebellows.ardublock.jbsensorboard;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import com.ardublock.translator.Translator;
import com.ardublock.translator.block.NumberBlock;
import com.ardublock.translator.block.TranslatorBlock;
import com.ardublock.translator.block.exception.BlockException;
import com.ardublock.translator.block.exception.SocketNullException;
import com.ardublock.translator.block.exception.SubroutineNotDeclaredException;

import edu.mit.blocks.codeblocks.BlockGenus;

public class SevenSegmentBlock extends TranslatorBlock {
	private String fileLocation = "info/thebellows/ardublock/jbsensorboard/seven_segment.c";
	public SevenSegmentBlock(Long blockId, Translator translator,
			String codePrefix, String codeSuffix, String label) {
		super(blockId, translator, codePrefix, codeSuffix, label);
	}

	@Override
	public String toCode() throws SocketNullException, SubroutineNotDeclaredException {
		TranslatorBlock translatorBlock = this.getRequiredTranslatorBlockAtSocket(0);
		
		String value = translatorBlock.toCode();
		
		URL fileURL = BlockGenus.class.getClassLoader().getResource(fileLocation);
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(fileURL.openStream()));
			String command = "", line;
			while((line = in.readLine()) != null) {
				command = command.concat(line + "\n");
			}
			
			translator.addDefinitionCommand(command);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		translator.addSetupCommand("TIMSK1 |= (1 << OCIE1A);\nTCCR1B = TCCR1B & 0b11111000 | 0x02;\n");
		
		return "seven_seg_sevenSegVal = constrain(" + value + ", 0, 99);\n";
	}

}

byte seven_seg_pins[] = {A4, A5, 0, 1, 2, 4, 7, 8};
byte seven_seg_digitGroups[][7] = {{0, 1, 2, 3, 4, 5, 6}, {0, 1, 2, 3, 4, 5, 7}};
byte seven_seg_ground[] = {7, 6};

byte seven_seg_num0[] = {0, 1, 2, 3, 4, 5};
byte seven_seg_num1[] = {1, 2};
byte seven_seg_num2[] = {0, 1, 3, 4, 6};
byte seven_seg_num3[] = {0, 1, 2, 3, 6};
byte seven_seg_num4[] = {1, 2, 5, 6};
byte seven_seg_num5[] = {0, 2, 3, 5, 6};
byte seven_seg_num6[] = {0, 2, 3, 4, 5, 6};
byte seven_seg_num7[] = {0, 1, 2};
byte seven_seg_num8[] = {0, 1, 2, 3, 4, 5, 6};
byte seven_seg_num9[] = {0, 1, 2, 5, 6};
int seven_seg_numArray[] = {(int)&seven_seg_num0, (int)&seven_seg_num1, (int)&seven_seg_num2, (int)&seven_seg_num3,
                  (int)&seven_seg_num4, (int)&seven_seg_num5, (int)&seven_seg_num6, (int)&seven_seg_num7,
                  (int)&seven_seg_num8, (int)&seven_seg_num9};
byte seven_seg_numSize[] = {6, 2, 5, 5, 4, 5, 6, 3, 7, 5};

byte seven_seg_digit = 0;
int seven_seg_sevenSegVal = 0;
int seven_seg_currentSevenSegVal = seven_seg_sevenSegVal;
byte seven_seg_segmentIndex = 0;

ISR(TIMER1_COMPA_vect){
  byte i = seven_seg_digit == 0 ? seven_seg_currentSevenSegVal / 10 : seven_seg_currentSevenSegVal % 10;

  for (int x = 0; x < 8; x++) {
		pinMode(seven_seg_pins[x], LOW);
    pinMode(seven_seg_pins[x], INPUT);
  }

  byte *t = (byte*)(seven_seg_numArray[i] + seven_seg_segmentIndex);

  pinMode(seven_seg_pins[seven_seg_digitGroups[seven_seg_digit][*t]], OUTPUT);
  digitalWrite(seven_seg_pins[seven_seg_digitGroups[seven_seg_digit][*t]], HIGH);
  pinMode(seven_seg_pins[seven_seg_ground[seven_seg_digit]], OUTPUT);
  digitalWrite(seven_seg_pins[seven_seg_ground[seven_seg_digit]], LOW);

  seven_seg_segmentIndex++;

  if (seven_seg_segmentIndex >= seven_seg_numSize[i]) {
	  seven_seg_segmentIndex = 0;
	  seven_seg_digit = !seven_seg_digit;
    if (seven_seg_digit == 0) {
    	seven_seg_currentSevenSegVal = seven_seg_sevenSegVal;
    }
  }
}
